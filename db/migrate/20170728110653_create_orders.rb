class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.integer :customer_id
      t.datetime :checked_out_at
      t.decimal :total_price

      t.timestamps
    end
    add_index :orders, :customer_id
    add_index :orders, :checked_out_at
  end
end
