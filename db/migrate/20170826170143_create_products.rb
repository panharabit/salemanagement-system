class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :product_name
      t.integer :product_unit
      t.decimal :product_price
      t.string :image
      t.references :category, foreign_key: true
      t.timestamps
    end
  end
end
