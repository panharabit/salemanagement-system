class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.string :username
      t.string :email
      t.string :password_hashL
      t.string :password_salt

      t.timestamps
    end
  end
end
