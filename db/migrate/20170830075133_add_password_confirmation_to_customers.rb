class AddPasswordConfirmationToCustomers < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :password_confirmation, :string
  end
end
