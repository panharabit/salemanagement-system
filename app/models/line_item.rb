class LineItem < ApplicationRecord
  #Association
  belongs_to :product
  belongs_to :order
end
