json.extract! product, :id, :product_name, :product_unit, :product_price, :image, :category_id, :created_at, :updated_at
json.url product_url(product, format: :json)
