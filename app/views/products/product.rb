class Product < ApplicationRecord
  belongs_to :category
  belongs_to :user
  has_many :line_items

#Validation
  validates :product_name, presence: true
  validates :product_unit, presence: true
  validates :product_price, presence: true

  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
