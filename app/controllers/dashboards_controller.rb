class DashboardsController < ApplicationController
  before_action :authenticate_user!
  before_action :admin_only, only:[:new,:edit,:update,:destroy]

  def index
    @users = User.all.limit(10)
    @products = Product.all.limit(10)
    @customers = Customer.all.limit(10)
    @orders = Order.all

    @usercount = User.all.count
    @productcount = Product.all.count
    @ordercount = Order.all.count
    @customercount = Customer.all.count

  end
end
