class HomesController < ApplicationController
  before_action :current_customer

  def index
    @products = Product.all
    @category = Category.all
  end


end
