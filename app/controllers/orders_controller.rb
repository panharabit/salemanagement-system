class OrdersController < ApplicationController
  before_action :authenticate_user!
  before_action :admin_only, only:[:new,:edit,:update,:destroy]

  def invoice
    @order = Order.find(params[:id])
    @lineitems = @order.line_items
    @fields = ["Item List","Category","Price"]
  end

  def index
    @orders = Order.all
    @fields = ["Order ID","State","Date","Customer","Total"]
  end

  def show
    @order = Order.find(params[:id])
    @lineitems = @order.line_items
    @fields = ["Product","Price"]
  end


end
