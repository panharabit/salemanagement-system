class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user! , only: [:new,:edit,:update,:destroy]
  before_action :admin_only, only: [:new,:edit,:update,:destroy]

  def product_detail
    @product = Product.find(params[:id])
  end

  # GET /products
  # GET /products.json
  def index
    @products = Product.all
    @fields = ["ID","Product Name","Unit","Price","Category","Image","Name"]
    #   if session[:cart_id]
    #   @cart = session[:cart_id] ? Order.find(session[:cart_id]) : current_customer.orders.build
    # else
    #   if @cart = session[:cart_id] ? Order.find(session[:cart_id]) : current_customer
    #     @cart = session[:cart_id] ? Order.find(session[:cart_id]) : current_customer.orders.build
    #   end
    # end
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @product = Product.find(params[:id])
    # @lines = LineItem.find(params[:id])
    # if session[:cart_id]
    #   @cart = session[:cart_id] ? Order.find(session[:cart_id]) : current_customer.orders.build
    # else
    #   if @cart = session[:cart_id] ? Order.find(session[:cart_id]) : current_customer
    #     @cart = session[:cart_id] ? Order.find(session[:cart_id]) : current_customer.orders.build
    #   end
    # end
  end

  # GET /products/new
  def new
    @product = current_user.products.build
  end
  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    @product =  current_user.products.build(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to products_url, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to products_url, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def admin_only
      unless current_user.admin?
        redirect_to root_path, :alert => "Access denied."
      end
    end

    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:product_name,:product_unit, :product_price, :image, :category_id)
    end
end
