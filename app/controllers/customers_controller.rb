class CustomersController < ApplicationController
  before_action :set_customer, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only:[:destroy]
  before_action :admin_only, only:[:destroy]
  before_action :login_required, only:[:edit,:customer_info,:edit_customer_info,:update_customer_info]
  before_action :current_customer, only:[:customer_info,:edit_customer_info,:update_customer_info]

  def customer_info
    @cust = Customer.find(params[:id])
    if @cust != current_customer
      render file: "#{Rails.root}/public/500.html", layout: false, status: 500
    else

    end
    @orders = @cust.orders

    @order = Order.find(params[:id])
    @lineitems = @order.line_items
    @fields = ["ID","Customer Name","Email","Image"]
  end

  def edit_customer_info
    @cust = Customer.find(params[:id])
  end

  def update_customer_info
    @cust = Customer.find(params[:id])
    if @cust != current_customer
      if @cust.update(customer_params)
        redirect_to :action => 'customer_info'
      else
        flash[:notice] = "All Fill Require"
        render 'edit_customer_info'
      end
    else
      flash[:notice] = "Your Attempt To Hack This Men Account"
      render 'edit_customer_info'
    end
  end

  def index
    @customers = Customer.all
    @fields = ["ID","Customer Name","Email","Image"]
  end

  def show
    @customer = Customer.find(params[:id])
    @orders = @customer.orders
    @fields = ["Order ID","State","Date","Total"]
  end

  def new
    @category = Category.all
    @customer = Customer.new
  end

  def create
    @category = Category.all
    @customer = Customer.new(customer_params)
    if @customer.save
      session[:customer_id] = @customer.id
      flash[:notice] = "Thank For Sign Up"
      redirect_to :homes
    else
      render :action => 'new'
    end
  end


  def edit
    @customer = Customer.find(params[:id])
  end

  def update
    @customer = Customer.find(params[:id])
    if session[:customer_id] == @customer.id
      if @customer.update(customer_params)
        redirect_to logout_path
      else
        flash[:notice] = "All Fill Require"
        render :edit
      end
    else
      flash[:notice] = "Your Attempt To Hack This Men Account"
      render :edit
    end
  end

  def destroy
    @customer.destroy
    redirect_to customers_url, notic: "Customer Has Been Deleted"
  end

  private
    def set_customer
      @customer = Customer.find(params[:id])
    end

    def admin_only
      unless current_user.admin?
        redirect_to root_path, :alert => "Access denied."
      end
    end

    def customer_params
      params.require(:customer).permit(:username,:email,:password,:salt,:encrypted_password)
    end
end
