class SessionsController < ApplicationController
  def new
  end

def create
  @category = Category.all
  customer = Customer.authenticate(params[:login],params[:password])
  if customer
    session[:customer_id] = customer.id
    flash[:notice] = "Logged In"
    redirect_to ("/")
  else
    flash.now[:error] = "Invalid Login or Password"
    render :action => 'new'
  end
end

def destroy
  session[:customer_id] =nil
  session.delete(:cart_id)
  flash[:notice] = "You have been logout"
  redirect_to "/"
end

end
