module ProductsHelper
  def product_category(product)
      if product.category.category_name == "Please Select Category"
        "Uncategory"
      else
        product.category.category_name
      end
    end

end
