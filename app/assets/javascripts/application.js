// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery

//= require tether
//= require bootstrap
//= require jquery_ujs
//= require_tree .
//= require moment
//= require bootstrap-datetimepicker
//
// $('#product_category_attributes_category_name').on('input', function() {
//
//    if($(this).val().length)
//       $('.select2-offscreen').prop('disabled', true);
//    else
//       $('.select2-offscreen').prop('disabled', false);
// });
//
// $('select').on('change', function() {
//    if($(this).val().length)
//     $('.disabled-form1').prop('disabled', true);
//    else
//     $('.disabled-form1').prop('disabled', false);
// })


$(document).ready(function(){
  $('.product-images').slick({
    dots: true
  });
});
