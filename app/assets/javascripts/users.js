$(document).ready(function() {
            // This will empty first option in select to enable placeholder
            $('#product_category_id option:first-child');
            // Select2
            $("select").select2({
                minimumResultsForSearch: 1
            });

            // Wizard With Form Validation
            $('.edit_user').bootstrapWizard({
                onTabShow: function(tab, navigation, index) {
                    tab.prevAll().addClass('done');
                    tab.nextAll().removeClass('done');
                    tab.removeClass('done');

                    var $total = navigation.find('li').length;
                    var $current = index + 1;

                    if($current >= $total) {
                        $('.edit_user').find('.wizard .next').addClass('hide');
                        $('#edit_user_1').find('.wizard .finish').removeClass('hide');
                    } else {
                        $('.edit_user').find('.wizard .next').removeClass('hide');
                        $('.edit_user').find('.wizard .finish').addClass('hide');
                    }
                },
                onTabClick: function(tab, navigation, index) {
                    return false;
                },
                onNext: function(tab, navigation, index) {
                    var $valid = $('.edit_user').valid();
                    if (!$valid) {
                        $validator.focusInvalid();
                        return false;
                    }
                }
            });

            // // This will submit the basicWizard form
            // $('.edit_user').submit(function() {
            // 		alert('This will submit the form wizard');
            // 		return false // remove this to submit to specified action url
            // });

            $('.fullscreen-link').on('click', function () {
              var ibox = $(this).closest('div.ibox');
              var button = $(this).find('i');
              $('body').toggleClass('fullscreen-ibox-mode');
              button.toggleClass('fa-expand').toggleClass('fa-compress');
              ibox.toggleClass('fullscreen');
              setTimeout(function () {
                $(window).trigger('resize');
            }, 100);
          });



        });

        $(document).ready(function() {
          $(function () {
              $('#datetimepicker1').datetimepicker();
          });
      });
