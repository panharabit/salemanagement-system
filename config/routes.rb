Rails.application.routes.draw do
    devise_for :users
    # resources :sessions

    root "homes#index"
    scope :admin do
      root "dashboards#index"
      resources :categories
      resources :products
      resources :users
      resources :customers
      resources :orders
      get "invoice/:id" => "orders#invoice", :as => :invoice
    end
    scope :shopping do
      resources :homes
      get "customer_info/:id" => "customers#customer_info", :as => :customer_info
      get "customer_info/:id/edit_customer_info" => "customers#edit_customer_info", :as => "edit_customer_info"
      patch "customer_info/:id" => "customers#update_customer_info"
      put "customer_info/:id" => "customers#update_customer_info"
      get "product_detail/:id" => "products#product_detail", :as => :product_detail
      resources :customers
    end



    mount ActionCable.server => '/cable'

    get "cart" => "cart#show"
    get "cart/add/:id" => "cart#add", :as => :add_to_cart
    post "cart/remove/:id" => "cart#remove", :as => :remove_from_cart
    post "cart/checkout" => "cart#checkout", :as => :checkout

    post 'session' => 'sessions#create', :as=> :session_customer
    get 'signup' => 'customers#new',:as=> :signup
    get 'logout' => 'sessions#destroy', :as => :logout
    get 'login' => 'sessions#new', :as => :login
    get "dashboard" => 'dashboards#index', :as => :dashboard_page

end
